<!DOCTYPE HTML>
<html lang="en">
	<title>php-signup-login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Unknown" >
	<meta name="Description" content="php-signup-login is a simple login/signup form with php & PostgreSQL.">
	<style>
		html {
			scroll-behaviour: smooth; }

		body {
			font-family: Courier New, courier;
			background: #212121;
			color: #64dd17; }

		h1, h2, h3, h4 ,h5 ,h6 {
			background: #00c853;
			color: #212121;
			font-weight: 500; }

		a, hr {
			color: #aeea00;
			font-weight: bold;
			text-decoration: none; }

		.signup-login-form h3 {
			background: #212121;
			color: #65dd17;	}

		* {box-sizing: border-box;}

		.wrapper {
			margin: 0 auto;
			display: grid;
			grid-template-columns: repeat(9, 1fr);
			grid-gap: 0px;
			grid-auto-rows: minmax(90px, auto); }

		.wrapper > div {
			padding: 1em; }

		.header {
			grid-column: 3 / 8;
			grid-row: 1;
			text-decoration: none;
			text-align: center; }

		.content {
			grid-column: 3 / 8;
			grid-row: 2; }

		.signup-login-form {
			grid-column: 4 / 7;
			grid-row: 3; }

		.footer {
			grid-column: 3 / 8;
			grid-row: 4;
			text-align: center; }

		@media only screen and (max-width: 1300px) {
			.header, .content, .footer {
				grid-column: 2 / 9; }

			.signup-login-form {
				grid-column: 3 / 8; } }

		@media only screen and (max-width: 768px) {
			.header, .content, .footer {
				grid-column: 1/ 10; }

			.signup-login-form {
				grid-column: 2 / 9; } }
	</style>
	</head>
	<body>
	<div class='wrapper'>
		<div class='header'>
			<h1>php-signup-login</h1>
		</div>
		<div class='content'>
			<?php
				echo "php-signup-login is a simple login/signup form with php & PostgreSQL.\n";
				?>
		</div>
		<div class='signup-login-form'>
			<h2>Signup/Login</h2>
			<form action='signup-login.php' method='post'>
				<h3>username<h3>
					<input type='text' name='username' required><br>
					<code>only letters, numbers and underscores are allowed.</code>

				<h3>password</h3>
					<input type='password' name='password' required><br>
					<code>must be longer than 7 characters.</code>

				<h3>email</h3>
					<input type='email' name='email' maxlength='256'><br>
					<code>you can request deletion from this address. (optional)</code>

				<h4>you should see a success message after submitting.</h4>
				<p><input type='submit' name='login' value='login'>  <input type='submit' name='signup' value='signup'></p>
			</form>
		</div>
		<div class='footer'>
			<code>Made for fun by Cyaniventer</code><br>
			<code>GPL-3.0-or-later</code>
		</div>
	</div>
	</body>
</html>
