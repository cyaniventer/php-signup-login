<!DOCTYPE HTML>
<html lang="en">
	<title>php-signup-login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Unknown" >
	<meta name="Description" content="php-signup-login is a simple login/signup form with php & PostgreSQL.">
	<style>
		html {
			scroll-behaviour: smooth; }

		body {
			font-family: Courier New, courier;
			background: #212121;
			color: #64dd17; }

		h1, h2, h3, h4 ,h5 ,h6 {
			background: #00c853;
			color: #212121;
			font-weight: 500; }

		a, hr {
			color: #aeea00;
			font-weight: bold;
			text-decoration: none; }

		.signup-login-form h3 {
			background: #212121;
			color: #65dd17;	}

		.reply {
			text-align: center; }

		* {box-sizing: border-box;}

		.wrapper {
			margin: 0 auto;
			display: grid;
			grid-template-columns: repeat(9, 1fr);
			grid-gap: 0px;
			grid-auto-rows: minmax(90px, auto); }

		.wrapper > div {
			padding: 1em; }

		.header {
			grid-column: 3 / 8;
			grid-row: 1;
			text-decoration: none;
			text-align: center; }

		.signup-login-form {
			grid-column: 4 / 7;
			grid-row: 2; }

		.footer {
			grid-column: 3 / 8;
			grid-row: 3;
			text-align: center; }

		@media only screen and (max-width: 1300px) {
			.header, .footer {
				grid-column: 2 / 9; }

			.signup-login-form {
				grid-column: 3 / 8; } }

		@media only screen and (max-width: 768px) {
			.header, .footer {
				grid-column: 1/ 10; }

			.signup-login-form {
				grid-column: 2 / 9; } }
	</style>
	</head>
	<body>
	<div class='wrapper'>
		<div class='header'>
			<h1>php-signup-login</h1>
		</div>
		<div class='signup-login-form'>
		<?php
			$errorMsg="<a href='index.php'>click here to try again.</a></p>";
			$dbh = new PDO('pgsql:dbname=php-signup-login;user=php-signup-login;password=oofeexaiphaiNg6iekiese0phaevohy5;');
			$userCheck = $_POST['username'];
				if (isset($_POST[signup])) {
					$stmt = $dbh->prepare("SELECT * FROM users WHERE username=?");
					$stmt->execute([$userCheck]);
					$user = $stmt->fetch();
					if ($user) {
						echo "<p class='reply'>this username is taken. $errorMsg";
						exit; }
					if ( preg_match('/\W/',$_POST['username']) ){
						echo "<p class='reply'>only letters, numbers and underscores are allowed for username. $errorMsg";
						exit; }
					if (strlen($_POST['password']) < 7) {
						echo "<p class='reply'>your password must be longer than 7 characters. $errorMsg";
						exit; }
					/**
					if (!preg_match("#[0-9]+#", $_POST['password'])) {
						echo "<p class='reply'>your password must include at least one number. $errorMsg";
						exit; }
					if (!preg_match("#[a-zA-Z]+#", $_POST['password'])) {
						echo "<p class='reply'>your password must include at least one letter. $errorMsg";
						exit; }
					*/
					//password, username checks are complete

					$options = [
						'threads' => 8,
						'memory_cost' => 8192,
						'time_cost' => 16, ];

					$hashedPass = password_hash($_POST['password'], PASSWORD_ARGON2ID, $options);
					$details = [
						'username' => $_POST['username'],
						'password' => $hashedPass,
						'email' => $_POST['email'],
						 ];
					$query = "INSERT INTO users (username, password, email) VALUES (:username, :password, :email)";
					$stmt= $dbh->prepare($query);
					$stmt->execute($details);

					//checks if username was created
					$stmt = $dbh->prepare("SELECT * FROM users WHERE username=?");
					$stmt->execute([$userCheck]);
					$user = $stmt->fetch();
					if ($user) {
						echo '<p class="reply">your account was created, now you can <a href="index.php">login</a>.</p>';
						exit; }
					else {
						echo "<p class='reply'>somewhere, something went wrong. you should contact the admin. $errorMsg";
						exit; }	}

				if (isset($_POST[login])) {
					$stmt = $dbh->prepare("SELECT * FROM users WHERE username=?");
					$stmt->execute([$userCheck]);
					$user = $stmt->fetch();
					if ($user) {
						if ($user && password_verify($_POST['password'], $user['password'])) {
							$escapedUser = htmlspecialchars($_POST['username']);
							echo "<p class='reply'>you are logged in as $escapedUser </p>\n";
							echo "<p class='reply'><a href='index.php'>click here to go home</a></p>"; }
						else {
							echo "<p class='reply'>you've entered the wrong password. $errorMsg"; }
						exit; }
					else {
						echo "<p class='reply'>this user doesn't exist. $errorMsg";
						exit; } }
				echo "<p class='reply'>if you see this message, contact the admin. $errorMsg";
			?>
		</div>
		<div class='footer'>
			<code>Made for fun by Cyaniventer</code><br>
			<code>GPL-3.0-or-later</code>
		</div>
	</div>
	</body>
</html>
